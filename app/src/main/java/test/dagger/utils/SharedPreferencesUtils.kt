package test.dagger.utils


import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class SharedPreferencesUtils private constructor(private val mContext: Context) {
    private val mSharedPreferences: SharedPreferences

    init {
        this.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
    }

    private object SingletonHolder {

        var instance: SharedPreferencesUtils? = null
        fun getInstance(context: Context): SharedPreferencesUtils {
            if (instance == null) {
                instance = SharedPreferencesUtils(context)
            }
            return instance
        }

    }

    companion object {

        fun getInstance(context: Context): SharedPreferencesUtils {
            PreferenceManager.getDefaultSharedPreferences(context)
            return SingletonHolder.getInstance(context)
        }
    }

}
