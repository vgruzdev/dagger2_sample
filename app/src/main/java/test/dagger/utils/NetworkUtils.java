package test.dagger.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import javax.inject.Inject;

public class NetworkUtils implements INetworkUtils {

    private final Context mContext;
    private final SharedPreferencesUtils mSharedPreferencesUtils;

    @Inject
    public NetworkUtils(Context context, SharedPreferencesUtils sharedPreferencesUtils) {
        this.mContext = context;
        this.mSharedPreferencesUtils = sharedPreferencesUtils;
    }

    public boolean isConnection() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public String checkInstanceOfSharedPreferences() {
        return mSharedPreferencesUtils == null ? "" : mSharedPreferencesUtils.toString();
    }

}
