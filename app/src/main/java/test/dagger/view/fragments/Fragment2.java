package test.dagger.view.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;

public class Fragment2 extends Fragment {

    public static Fragment2 newInstance() {
        
        Bundle args = new Bundle();
        
        Fragment2 fragment = new Fragment2();
        fragment.setArguments(args);
        return fragment;
    }
    
}
