package test.dagger.view.presenters;


import android.util.Log;

import test.dagger.utils.DatabaseUtils;
import test.dagger.utils.NetworkUtils;

public class SCPresenter {

    private NetworkUtils networkUtils;
    private DatabaseUtils databaseHelper;

    public SCPresenter(NetworkUtils networkUtils, DatabaseUtils databaseHelper) {
        this.networkUtils = networkUtils;
        this.databaseHelper = databaseHelper;
    }

    public void getHistoryChat(){
        if(networkUtils.isConnection()) {
            Log.e("history", "chat");
        }
    }

}
