package test.dagger.view.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import test.dagger.R;
import test.dagger.dagger.App;
import test.dagger.utils.IActivityUtils;
import test.dagger.utils.IDatabaseUtils;
import test.dagger.utils.INetworkUtils;
import test.dagger.utils.PresenterActivityUtils;
import test.dagger.utils.SharedPreferencesUtils;

public class Activity1 extends DaggerAppCompatActivity {

    @Inject
    IActivityUtils mActivityUtils;

    @Inject
    PresenterActivityUtils mPresenterActivityUtils;

    @Inject
    SharedPreferencesUtils mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        App.plusActivityComponent().inject(this);
        Log.d(App.TAG, "test activity1 mActivityUtils " + mActivityUtils + " mPresenterActivityUtils " + mPresenterActivityUtils + " mSharedPreferences " + mSharedPreferences);
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Activity2.class);
                startActivity(i);
            }
        });
    }

}
