package test.dagger.view.activities;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import javax.inject.Inject;

import test.dagger.R;
import test.dagger.dagger.App;
import test.dagger.utils.IActivityUtils;
import test.dagger.utils.PresenterActivityUtils;

public class Activity2 extends AppCompatActivity {


    @Inject
    IActivityUtils mActivityUtils;

    @Inject
    PresenterActivityUtils mPresenterActivityUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        App.plusActivityComponent().inject(this);
        Log.d(App.TAG, "test activity2 mActivityUtils " + mActivityUtils + " mPresenterActivityUtils " + mPresenterActivityUtils);
    }
}
