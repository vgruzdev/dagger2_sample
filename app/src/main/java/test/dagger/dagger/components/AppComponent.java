package test.dagger.dagger.components;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import test.dagger.dagger.App;
import test.dagger.dagger.modules.activityComponents.ActivityModule;
import test.dagger.dagger.modules.activityComponents.ActivityPresenterModule;
import test.dagger.dagger.modules.appComponents.AppModule;
import test.dagger.dagger.modules.appComponents.DataModule;
import test.dagger.dagger.modules.appComponents.NetworkUtilsModule;
import test.dagger.view.activities.Activity1;

@Singleton
@Component(modules = {AppModule.class, DataModule.class, NetworkUtilsModule.class})
public interface AppComponent {

    ActivityComponent plusActivityComponent(ActivityModule activityModule, ActivityPresenterModule activityPresenterModule);

    void inject(App app);

}
