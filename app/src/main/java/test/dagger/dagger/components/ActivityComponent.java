package test.dagger.dagger.components;


import dagger.Subcomponent;
import test.dagger.dagger.customScope.ActivityScope;
import test.dagger.dagger.modules.activityComponents.ActivityModule;
import test.dagger.dagger.modules.activityComponents.ActivityPresenterModule;
import test.dagger.dagger.modules.fragmentComponent.FragmentModule;
import test.dagger.view.activities.Activity1;
import test.dagger.view.activities.Activity2;

@ActivityScope
@Subcomponent(modules = {ActivityModule.class, ActivityPresenterModule.class})
public interface ActivityComponent {

    FragmentComponent.Builder fragmentComponentBuilder();
//    FragmentComponent plusFragmentComponent(FragmentModule fragmentModule);

    void inject(Activity1 activity1);
    void inject(Activity2 activity2);

}
