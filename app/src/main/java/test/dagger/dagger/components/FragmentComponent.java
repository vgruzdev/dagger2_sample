package test.dagger.dagger.components;


import dagger.Subcomponent;
import test.dagger.dagger.customScope.FragmentScope;
import test.dagger.dagger.modules.fragmentComponent.FragmentModule;
import test.dagger.view.fragments.Fragment1;

@FragmentScope
@Subcomponent(modules = {FragmentModule.class})
public interface FragmentComponent {

    @Subcomponent.Builder
    interface Builder {
        FragmentComponent.Builder fragmentModule(FragmentModule fragmentModule);
        FragmentComponent build();
    }

    void inject(Fragment1 fragment1);

}
