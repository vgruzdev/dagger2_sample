package test.dagger.dagger;

import android.app.Application;
import android.util.Log;

import javax.inject.Inject;

import test.dagger.dagger.components.ActivityComponent;
import test.dagger.dagger.components.AppComponent;
import test.dagger.dagger.components.DaggerAppComponent;
import test.dagger.dagger.components.FragmentComponent;
import test.dagger.dagger.modules.activityComponents.ActivityModule;
import test.dagger.dagger.modules.activityComponents.ActivityPresenterModule;
import test.dagger.dagger.modules.appComponents.AppModule;
import test.dagger.dagger.modules.appComponents.DataModule;
import test.dagger.dagger.modules.appComponents.NetworkUtilsModule;
import test.dagger.dagger.modules.fragmentComponent.FragmentModule;
import test.dagger.utils.IDatabaseUtils;
import test.dagger.utils.INetworkUtils;
import test.dagger.utils.SharedPreferencesUtils;

public class App extends Application {

    public static final String TAG = "NewTest";

    private static AppComponent component;
    private static ActivityComponent activityComponent;
    private static FragmentComponent fragmentComponent;

    /**
     * Injection
     */

    @Inject
    INetworkUtils mNetworkUtils;

    @Inject
    IDatabaseUtils mDatabaseHelper;

    @Inject
    SharedPreferencesUtils mSharedPreferencesUtils;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .dataModule(new DataModule())
                .networkUtilsModule(new NetworkUtilsModule())
                .build();



        testInjectionAppComponent();
    }

    public static ActivityComponent plusActivityComponent() {
        if (activityComponent == null) {
            activityComponent = component.plusActivityComponent(new ActivityModule(), new ActivityPresenterModule());
        }
        return activityComponent;
    }

    public static void cleanActivityComponent() {
        activityComponent = null;
    }

    public static FragmentComponent plusFragmentComponent() {
        if (fragmentComponent == null) {
            fragmentComponent = activityComponent.fragmentComponentBuilder().fragmentModule(new FragmentModule()).build();
        }
        return fragmentComponent;
    }

    public static void cleanFragmentComponent() {
        fragmentComponent = null;
    }

    private void testInjectionAppComponent() {
        component.inject(this);
        Log.d(TAG, "app mNetworkUtils " + mNetworkUtils + " mDatabaseHelper " + mDatabaseHelper + " mSharedPreferencesUtils " + mSharedPreferencesUtils);
        Log.d(TAG, "app mNetworkUtilsmSharedPreferencesUtils " + mNetworkUtils.checkInstanceOfSharedPreferences());
    }

    public static AppComponent getComponent() {
        return component;
    }

}
