package test.dagger.dagger.modules.appComponents;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import test.dagger.utils.DatabaseUtils;
import test.dagger.utils.IDatabaseUtils;
import test.dagger.utils.SharedPreferencesUtils;

@Module
public class DataModule {

    @Provides
    @NonNull
    @Singleton
    IDatabaseUtils provideDatabaseUtils() {
        return new DatabaseUtils();
    }

    @Provides
    @NonNull
    @Singleton
    SharedPreferencesUtils provideSharedPreferencesUtils(Context context) {
        return SharedPreferencesUtils.Companion.getInstance(context);
    }


}
