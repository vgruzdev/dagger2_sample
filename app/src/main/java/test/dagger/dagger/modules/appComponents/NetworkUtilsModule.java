package test.dagger.dagger.modules.appComponents;


import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import test.dagger.utils.INetworkUtils;
import test.dagger.utils.NetworkUtils;
import test.dagger.utils.SharedPreferencesUtils;

@Module
public class NetworkUtilsModule {

    @Provides
    @Singleton
    INetworkUtils provideNetworkUtils(
            Context context,
            SharedPreferencesUtils sharedPreferencesUtils){
        return new NetworkUtils(context, sharedPreferencesUtils);
    }

}
