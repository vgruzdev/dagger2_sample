package test.dagger.dagger.modules.activityComponents;


import android.content.Context;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import test.dagger.dagger.customScope.ActivityScope;
import test.dagger.utils.ActivityUtils;
import test.dagger.utils.IActivityUtils;

@Module
public class ActivityModule {

    @Provides
    @NonNull
    @ActivityScope
    public IActivityUtils provideActivityUtils(Context context) {
        return new ActivityUtils(context);
    }

}
