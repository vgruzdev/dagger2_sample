package test.dagger.dagger.modules.fragmentComponent;


import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import test.dagger.dagger.customScope.FragmentScope;
import test.dagger.utils.IActivityUtils;
import test.dagger.view.presenters.FragmentPresenter;

@Module
public class FragmentModule {

    @Provides
    @NonNull
    @FragmentScope
    FragmentPresenter provideFragmentPresenter(IActivityUtils activityUtils) {
        return new FragmentPresenter(activityUtils);
    }

}
