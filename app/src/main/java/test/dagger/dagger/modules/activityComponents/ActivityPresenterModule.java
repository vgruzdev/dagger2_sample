package test.dagger.dagger.modules.activityComponents;


import android.content.Context;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import test.dagger.dagger.customScope.ActivityScope;
import test.dagger.utils.PresenterActivityUtils;

@Module
public class ActivityPresenterModule {

    @Provides
    @NonNull
    @ActivityScope
    PresenterActivityUtils providePresenterActivityUtils(Context context) {
        return new PresenterActivityUtils(context);
    }

}
